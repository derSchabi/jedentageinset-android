package org.schabi.jedentageinset

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Request.Builder
import org.jsoup.Jsoup


class Crawler {

    class SetItem(title : String, imgUrl : String, link : String, description : String) {
        val title = title
        val imgUrl = imgUrl
        val link = link
        val description = description
    }

    class SetInfo(site : String, streamLink : String) {
        val site = site
        val streamLink = streamLink
    }

    val client = OkHttpClient()

    fun getItems(page : Int) : Single<List<SetItem>> {
        assert(page > 0)
        val url = "https://jedentageinset.de/page/" + page.toString()
        return Single.create { emitter ->
            val request: Request = Builder()
                .url(url)
                .build()
            val rawPage = client.newCall(request).execute().use { response -> response.body!!.string() }
            val page = Jsoup.parse(rawPage, url)

            val postContent = page.getElementById("posts_cont")

            val setItems = mutableListOf<SetItem>()
            for (homeBox in  postContent.getElementsByClass("home_box")) {
                val a = homeBox.getElementsByTag("a")
                val link = a.attr("href")
                val title = a.text()
                val imgUrl = homeBox.getElementsByTag("img").attr("src")
                val description = homeBox.getElementsByTag("p").text()
                setItems.add(SetItem(title, imgUrl, link, description))
            }
            emitter.onSuccess(setItems)
        }
    }

    fun getContent(link: String) : Single<SetInfo> {
        assert(link.startsWith("https://"))
        return Single.create { emitter ->
            val request = Builder()
                .url(link)
                .build()
            val rawPage = client.newCall(request).execute().use { response ->
                response.body!!.string()
            }
            val page = Jsoup.parse(rawPage, link)
            val single_content = page.getElementById("single_cont")
                .getElementsByClass("single_left").first()
            val title = single_content.getElementsByClass("single_title").first()
            val date = single_content.getElementsByClass("post_date").first()
            val content = single_content.getElementsByClass("single_inside_content").first()

            val iframes = page.getElementsByTag("iframe")

            var soundCloundPlayerUrl : String? = null
            for(iframe in iframes) {
                val srcLink = iframe.attr("src")
                if(srcLink.startsWith("https://w.soundcloud.com/player/")) {
                    soundCloundPlayerUrl = srcLink
                }
            }

            val playerRequest: Request = Builder()
                .url(soundCloundPlayerUrl!!)
                .build()
            val rawPlayer = client.newCall(playerRequest).execute().use { response ->
                response.body!!.string()
            }
            val playerPage = Jsoup.parse(rawPlayer, soundCloundPlayerUrl)
            val setLinkElement = playerPage.select("link[rel=canonical]")
            val streamLink = setLinkElement.attr("href")


            //remove pintareset stuff
            content.getElementsByClass("wpsso-pinterest-pin-it-image").remove()

            val content_html = title.outerHtml() + date.outerHtml() + content.outerHtml()

            println(title.html())

            emitter.onSuccess(SetInfo(content_html, streamLink ))
        }

    }
}